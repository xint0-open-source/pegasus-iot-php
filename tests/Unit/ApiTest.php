<?php

declare(strict_types=1);

namespace Tests\Unit;

use PHPUnit\Framework\Attributes\CoversClass;
use Xint0\PegasusPhp\Api;
use PHPUnit\Framework\TestCase;

#[CoversClass(Api::class)]
class ApiTest extends TestCase
{
    public function test_can_crate_instance(): void
    {
        $sut = new Api();
        $this->assertNotNull($sut);
    }
}
