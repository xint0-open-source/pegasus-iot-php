<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/pegasus-iot-php/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Tests\Unit\Requests;

use Http\Discovery\Psr17FactoryDiscovery;
use PHPUnit\Framework\Attributes\CoversClass;
use Xint0\PegasusPhp\Requests\ViewGeofences;
use PHPUnit\Framework\TestCase;

#[CoversClass(ViewGeofences::class)]
class ViewGeofencesTest extends TestCase
{
    public function test_make_method_of_default_instance_returns_request_interface_with_expected_method_and_uri(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('GET', $actual->getMethod());
        $this->assertSame($baseUri->getScheme(), $actualUri->getScheme());
        $this->assertSame($baseUri->getHost(), $actualUri->getHost());
        $this->assertSame('/api/geofences', $actualUri->getPath());
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_page_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withPage(5)
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('page=5', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_negative_page_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withPage(-5)
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_zero_page_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withPage(0)
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_page_size_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withPageSize(5)
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('set=5', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_negative_page_size_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withPageSize(-5)
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_zero_page_size_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withPageSize(0)
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_search_expression_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSearch('geometry.type', 'Polygon')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('search.geometry.type=Polygon', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_multiple_search_expressions_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSearch('geometry.type', 'Polygon')
            ->withSearch('properties.name', 'Test')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('search.geometry.type=Polygon&search.properties.name=Test', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_search_expression_with_empty_path_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = (new ViewGeofences());
        $actual = $sut
            ->withSearch('', 'Test')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_single_select_property_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = (new ViewGeofences());
        $actual = $sut
            ->withSelect('geometry,properties')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('select=geometry%2Cproperties', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_single_empty_select_property_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSelect('')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_empty_and_all_spaces_select_property_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSelect('', '    ')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_select_property_with_leading_and_trailing_spaces_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSelect(' geometry  ', ' properties ')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('select=geometry%2Cproperties', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_duplicate_select_properties_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSelect(' geometry  ', ' geometry ', 'geometry', 'properties', ' id', ' properties ')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('select=geometry%2Cproperties%2Cid', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_multiple_empty_select_properties_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSelect('', 'geometry', '')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('select=geometry', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_all_spaces_select_property_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSelect('   ')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_multiple_select_properties_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withSelect('id', 'geometry', 'properties')
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('select=id%2Cgeometry%2Cproperties', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_modified_before_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withModifiedBefore(new \DateTimeImmutable('2024-05-01 12:00:00'))
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('modified_before=2024-05-01T12%3A00%3A00', $actualUri->getQuery());
    }

    public function test_make_method_of_instance_with_modified_after_returns_request_interface_with_expected_query_string(): void
    {
        $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://mx.fleetmetriks.com/');
        $sut = new ViewGeofences();
        $actual = $sut
            ->withModifiedAfter(new \DateTimeImmutable('2024-05-01 12:00:00'))
            ->make($baseUri);
        $actualUri = $actual->getUri();
        $this->assertSame('modified_after=2024-05-01T12%3A00%3A00', $actualUri->getQuery());
    }
}
