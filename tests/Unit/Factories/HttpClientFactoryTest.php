<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/pegasus-iot-php/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Tests\Unit\Factories;

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use Xint0\PegasusPhp\Factories\HttpClientFactory;
use PHPUnit\Framework\TestCase;

#[CoversClass(HttpClientFactory::class)]
class HttpClientFactoryTest extends TestCase
{
    public function test_create_returns_http_client_that_sends_requests_with_expected_headers(): void
    {
        $mockClient = new Client();
        $mockRequest = Psr17FactoryDiscovery::findRequestFactory()->createRequest('GET', 'https://mx.fleetmetriks.com/api/devices');
        $sut = new HttpClientFactory();

        $client = $sut->create($mockClient, 'token');

        $client->sendRequest($mockRequest);

        $actualRequest = $mockClient->getLastRequest();

        $this->assertArrayHasKey('Authenticate', $actualRequest->getHeaders());
        $this->assertSame('token', $actualRequest->getHeaderLine('Authenticate'));
        $this->assertArrayHasKey('Content-Length', $actualRequest->getHeaders());
        $this->assertSame('0', $actualRequest->getHeaderLine('Content-Length'));
    }

    public function test_create_without_authentication_returns_http_client_that_sends_requests_without_authenticate_header(): void
    {
        $mockClient = new Client();
        $mockRequest = Psr17FactoryDiscovery::findRequestFactory()->createRequest('GET', 'https://mx.fleetmetriks.com/api/');
        $sut = new HttpClientFactory();

        $client = $sut->create($mockClient);

        $client->sendRequest($mockRequest);

        $actualRequest = $mockClient->getLastRequest();

        $this->assertArrayNotHasKey('Authenticate', $actualRequest->getHeaders());
        $this->assertArrayHasKey('Content-Length', $actualRequest->getHeaders());
        $this->assertSame('0', $actualRequest->getHeaderLine('Content-Length'));
    }
}
