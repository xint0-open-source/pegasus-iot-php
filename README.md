# Pegasus IoT PHP client

## Description
PHP client for the Pegasus IoT Cloud API.

## Installation

### Requirements

- PHP 8.2 or later
- PSR-18 client implementation

### Install using Composer

```bash
composer require xint0/pegasus-iot-php
```

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License
The Pegasus IoT Cloud PHP client is open source software licensed under the [MIT License](https://gitlab.com/xint0-open-source/pegasus-iot-php/-/blob/main/LICENSE).

## Project status
