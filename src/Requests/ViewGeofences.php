<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/pegasus-iot-php/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Xint0\PegasusPhp\Requests;

use DateTimeInterface;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Request to view set of geofences.
 */
class ViewGeofences
{
    private int $page = 0;
    private int $pageSize = 0;
    private array $search = [];
    private array $selectProperties = [];
    private ?DateTimeInterface $modifiedAfter = null;
    private ?DateTimeInterface $modifiedBefore = null;

    public function make(UriInterface $baseUri, ?RequestFactoryInterface $requestFactory = null): RequestInterface
    {
        $requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $uri = $baseUri->withPath('/api/geofences')
            ->withQuery(http_build_query($this->getQueryParams()));
        return $requestFactory->createRequest('GET', $uri);
    }

    public function withModifiedAfter(DateTimeInterface $modifiedAfter): static
    {
        $new = clone $this;
        $new->modifiedAfter = $modifiedAfter;
        return $new;
    }

    public function withModifiedBefore(DateTimeInterface $modifiedBefore): ViewGeofences
    {
        $new = clone $this;
        $new->modifiedBefore = $modifiedBefore;
        return $new;
    }

    public function withPage(int $page): ViewGeofences
    {
        $new = clone $this;
        $new->page = max($page, 0);
        return $new;
    }

    public function withPageSize(int $size): ViewGeofences
    {
        $new = clone $this;
        $new->pageSize = max($size, 0);
        return $new;
    }

    public function withSearch(string $path, string $value): ViewGeofences
    {
        $new = clone $this;
        $path = trim($path);
        if ($path) {
            $new->search[$path] = trim($value);
        }
        return $new;
    }

    public function withSelect(string ...$properties): ViewGeofences
    {
        $new = clone $this;
        $new->selectProperties = array_unique(array_filter(array_map('\\trim', $properties)));
        return $new;
    }

    private function getQueryParams(): array
    {
        $params = [];
        if ($this->page > 0) {
            $params['page'] = $this->page;
        }
        if ($this->pageSize > 0) {
            $params['set'] = $this->pageSize;
        }
        if ($this->search) {
            foreach ($this->search as $path => $value) {
                $params["search.$path"] = $value;
            }
        }
        if ($this->selectProperties) {
            $params['select'] = join(',', $this->selectProperties);
        }
        if (null !== $this->modifiedBefore) {
            $params['modified_before'] = $this->modifiedBefore->format('Y-m-d\TH:i:s');
        }
        if (null !== $this->modifiedAfter) {
            $params['modified_after'] = $this->modifiedAfter->format('Y-m-d\TH:i:s');
        }
        return $params;
    }
}