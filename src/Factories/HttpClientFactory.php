<?php

/**
 * @copyright 2024 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/pegasus-iot-php/-/blob/main/LICENSE MIT
 */

declare(strict_types=1);

namespace Xint0\PegasusPhp\Factories;

use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Common\Plugin\ContentLengthPlugin;
use Http\Client\Common\PluginClient;
use Http\Discovery\Psr18ClientDiscovery;
use Http\Message\Authentication\Header;
use Psr\Http\Client\ClientInterface;

/**
 * Creates HTTP client instance optionally using credentials from specified credential store.
 *
 * @author Rogelio Jacinto <rogelio.jacinto@gmail.com>
 */
class HttpClientFactory
{
    public function create(?ClientInterface $client = null, ?string $session_token = null): ClientInterface
    {
        $client = $client ?? Psr18ClientDiscovery::find();
        $plugins = [];
        if (null !== $session_token) {
            $plugins[] = new AuthenticationPlugin(new Header('Authenticate', $session_token));
        }
        $plugins[] = new ContentLengthPlugin();
        return new PluginClient($client, $plugins);
    }
}
